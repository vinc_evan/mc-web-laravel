<!DOCTYPE html>
    <head>
        <title>{{$title}} | Account</title>
        @include('templates/bs_head')
        <link rel="stylesheet" href="{{asset('/assets/css/staticpage.css')}}">
        <link rel="stylesheet" href="{{asset('/assets/css/dashboard.css')}}">
    </head>

    <body>
        <!--Navigation Bar-->
        <nav id="main-navbar" class="navbar navbar-expand-sm navbar-dark ">
        <a class="navbar-brand" href="{{url('home')}}">TitleCraft</a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarTogglerDemo02" aria-controls="navbarTogglerDemo02" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarTogglerDemo02">
                <ul class="navbar-nav ml-auto mt-2 mt-lg-0 ">
                    <li class="nav-item dropdown">
                        <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                            {{ Auth::user()->username }} <span class="caret"></span>
                        </a>

                        <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                            <a class="dropdown-item" href="{{ route('logout') }}"
                               {{-- onclick="event.preventDefault();
                                             document.getElementById('logout-form').submit();"> --}}>
                                {{ __('Logout') }}
                            </a>

                            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                @csrf
                            </form>
                        </div>
                    </li>
                </ul>
            </div> 
        </nav>
        <!--End of Nav Bar-->

        {{-- Banner and Page Title --}}
        <div class="container-fluid d-flex align-items-center mc-pg-banner">
            <div class="row">
                <div class="col text-white">
                    <h2>Account</h2>
                </div>
            </div>
        </div>

        <div class="container mc-dashboard">
            {{-- SideBar --}}
            <div class="row mx-0">
                <nav id="sidemenu" class="col-lg-2 col-md-4 {{-- col-md-2 d-none d-md-block--}} mc-sidebar">
                    <div class="sidebar-sticky">
                        <p class="d-none d-md-block">Hello, {{Auth::user()->username}}</p>
                        <ul class="nav flex-md-column justify-content-around">
                            <li class="nav-item"> <a href="">Account</a> </li>
                            <li class="nav-item"> <a href="">Account Settings</a> </li>
                            <li class="nav-item"> <a href="{{ route('logout') }}" class="text-danger">Logout</a> 
                            </li>
                        </ul>
                    </div>
                </nav>
                <div class="col mc-content pt-4 pt-md-0">
                    <div class="card">
                        <div class="card-body">
                            <h4 class="card-title">Change Password</h4>
                            <hr class="light-hr">
                            <form class="" method="POST">
                                @csrf
                                <div class="form-group row">
                                    <div class="col-md-3">
                                        <span class="">Old Password:</span>
                                    </div>
                                    <div class="col-md">
                                        <input id="old_password" placeholder="" type="password" class="" name="old_password" value="" autofocus>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="col-md-3">
                                        <span class="">New Password:</span>
                                    </div>
                                    <div class="col-md">
                                        <input id="new_password" placeholder="" type="password" class="" name="new_password" value="" >
                                        
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="col-md-3">
                                        <span class="">Confirm Password:</span>
                                    </div>
                                    <div class="col-md">
                                        <input id="confirm_password" placeholder="" type="password" class="" name="confirm_password" value="" >
                                        
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="col-md">
                                        <button type="submit" class="btn btn-primary">
                                            Submit
                                        </button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        @include('templates/overlay_discord')
        @include('templates/footer')
    </body>
    @include('templates/bs_foot')

</html>