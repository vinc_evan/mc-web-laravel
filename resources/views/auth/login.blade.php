<!DOCTYPE html>
    <head>
        <title>{{$title}} | Sign in</title>
        @include('templates/bs_head')
        <link rel="stylesheet" href="{{asset('/assets/css/authpage.css')}}"/>
    </head>

    <body>

        <nav id="main-navbar" class="navbar fixed-top navbar-expand-lg navbar-dark bg-transparent">
            <a class="navbar-brand" href="{{url('home')}}">{{$title}}</a>

            <ul class="navbar-nav ml-auto">
                <li class="nav-item">
                    <a class="nav-link text-light" href="{{url('register')}}">Sign Up</a>
                </li>
            </ul>
        </nav>

        <div class="container-fluid d-flex mc-auth align-content-stretch">
            <div class="flex-row d-flex align-items-center justify-content-center w-100">
                <div class="flex-column">
                    <div class="card mc-auth-card"> 
                        <div class="card-body">
                            <h4 class="card-title text-center">Sign In</h4>
                            <hr class="light-hr">
                            <form class="" method="POST" action="{{ route('login') }}">
                                @csrf

                                <div class="form-group row">
                                    <div class="col-md">
                                        <input id="username" placeholder="Username" type="text" class="form-control @error('username') is-invalid @enderror" name="username" value="{{ old('username') }}" required autocomplete="username" autofocus>
                                        @error('username')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
                                </div>
        
                                <div class="form-group row">
                                    <div class="col-md-12">
                                        <input id="password" placeholder="Password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="current-password">

                                        @error('password')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
                                </div>

        
                                <div class="form-group row d-flex justify-content-between">
                                    <div class="col-md-5">
                                        <div class="form-check">
                                            <input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>
        
                                            <label class="form-check-label" for="remember">
                                                {{ __('Remember Me') }}
                                            </label>
                                        </div>
                                    </div>
                                </div>
        
        
                                <div class="form-group row">
                                    <div class="col-md">
                                        <button type="submit" class="btn btn-lg btn-primary btn-block text-uppercase">
                                            {{ __('Sign In') }}
                                        </button>
                                    </div>
                                </div>

                                <div class="text-center mc-auth-subtext">
                                    <span>Don't have an account? <a href="{{url('register')}}">Sign Up</a></span><br>
                                    {{-- @if (Route::has('password.request'))
                                    <a class="" href="{{ route('password.request') }}">
                                        {{ __('Forgot Your Password?') }}
                                    </a>
                                    @endif --}}
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </body>

</html>