<!DOCTYPE html>
    <head>
        <title>{{$title}} | Registration</title>
        @include('templates/bs_head')
        <link rel="stylesheet" href="{{asset('/assets/css/authpage.css')}}"/>
    </head>

    <body>
        <nav id="main-navbar" class="navbar fixed-top navbar-expand-lg navbar-dark bg-transparent">
            <a class="navbar-brand" href="{{url('home')}}">{{$title}}</a>

            <ul class="navbar-nav ml-auto">
                <li class="nav-item">
                    <a class="nav-link text-light" href="{{url('login')}}">Sign In</a>
                </li>
            </ul>   
        </nav>

        <div class="container-fluid d-flex mc-auth align-content-stretch">
            <div class="flex-row d-flex align-items-center justify-content-center w-100">
                <div class="flex-column">
                    <div class="card mc-auth-card">
                        <div class="card-body">
                            <h4 class="card-title text-center">Sign Up</h4>
                            <hr class="light-hr">
                            <form class="" method="POST" action="{{ route('register') }}">
                                @csrf

                                <div class="form-group row">
                                    <div class="col-md">
                                        <input id="fullname" type="text" value="" class="form-control @error('fullname') is-invalid @enderror" name="fullname" value="{{ old('fullname') }}" placeholder="Fullname" required autofocus>
                                        @error('fullname')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="col-md">
                                        <input id="email" placeholder="E-mail" value="{{$mail ?? ''}}" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email">

                                        @error('email')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="col-md-12">
                                        <input id="username" type="text" value="{{$uname ?? ''}}" placeholder="Username" class="form-control @error('username') is-invalid @enderror" name="username" value="{{ old('username') }}" required>

                                        @error('username')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
                                    <span class="mc-auth-subtext text-info font-weight-bold" style="clear:both">Please use your minecraft username, this can not be changed.</span>
                                </div>
        
                                <div class="form-group row">
                                    <div class="col-md">
                                        <input id="password" placeholder="Password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="new-password">
        
                                        @error('password')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
                                </div>
        
                                <div class="form-group row">
                                    <div class="col-md">
                                        <input id="password-confirm" placeholder="Confirm Password" type="password" class="form-control" name="password_confirmation" required autocomplete="new-password">
                                    </div>
                                </div>
        
                                <div class="form-group row">
                                    <div class="col-md">
                                        <button type="submit" class="btn btn-lg btn-primary btn-block text-uppercase">
                                            {{ __('Register') }}
                                        </button>
                                    </div>
                                </div>

                                <div class="mc-auth-subtext text-center">
                                    <span>By signing up, you agree to the TOS.</span><br>
                                    <span>This registration includes in-game authentication, you can use this credentials to enter the game.</span>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </body>

</html>