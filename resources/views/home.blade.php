<!DOCTYPE html>
    <head>
        <title>{{$title}}</title>
        @include('templates/bs_head')
        <style>
            .mc-banner{
                height: 100%;
                /* background:url("{{asset('/public/assets/img/bg_banner.png')}}") no-repeat bottom center; */
                padding: 10% 5%;
                display:inline-block;
                align-items:center;
            }
        </style>
    </head>
    <body>
        <!--Navigation Bar-->
        <nav id="main-navbar" class="navbar fixed-top navbar-expand-sm navbar-dark ">
            @include('templates/navbar-inner')
        </nav>
        <!--End of Nav Bar-->

        {{-- Banner --}}
        <div class="container-fluid mc-banner" >
            <div class="row mc-inner-banner align-self-center" style="">
                <div class="col offset-lg-1">
                    <div class="d-flex justify-content-around align-content-center">
                        <div class="col align-self-center">
                            <h3 class="page-title align-self-center">Survival Server. Simplified.</h3>
                        </div>
                        <div class="col-lg-5 d-none d-lg-flex align-content-center">
                            {{-- Login card --}}
                            <div class="card regist-form align-self-center">
                                <h4 class="card-title text-center" style="padding:0 0 15px 0">Register Now</h4>
                                <p class="text-muted text-center" style="margin-top:-25px; margin-bottom:10px;"><small>Register here, in-game auth included!</small></p>
                                <form method="post" action=""{{route('store.data')}}>
                                    {{csrf_field()}}
                                    <div class="form-group input-group">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text text-center"> <i class="fas fa-envelope w-100"></i></span>
                                        </div>
                                        <input id="input_email" name="mail" class="form-control" placeholder="E-mail" type="text">
                                    </div>
            
                                    <div class="form-group input-group">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text text-center"> <i class="fas fa-user w-100"></i></span>
                                        </div>
                                        <input id="input_username" name="uname" class="form-control" placeholder="Username" type="text">
                                    </div>

                                    <p class="text-muted" style="margin-top:-15px; margin-bottom:-10px;"><small>Use your Minecraft username to Sign Up.</small></p>
{{--                                     
                                    <div class="form-group input-group">
                                        <div class="input-group-prepend">
                                                <span class="input-group-text"> <i class="fa fa-lock w-100"></i></span>
                                        </div>
                                        <input id="" name="" class="form-control" placeholder="Password" type="password">
                                    </div> --}}

                                    
                                    <div class="align-items-center text-center" style="padding-top:20px;">
                                        {{-- <button id="home-sign-up" type="button" class="btn btn-success btn-block">Create Account</button> --}}
                                        <input type="submit" class="btn btn-success btn-block" value="Sign Up">
                                        <p style="padding-top:5px;">Already a member?
                                            <a href="{{url('login')}}">Sign In</a>
                                        </p>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
                
            </div>
        </div>

        {{-- Join Us --}}
        <div class="container-fluid section-join text-center ">
            <h3>Join Now!</h3>
            <hr style="border-color:#888; width:50%;">
            <p style="font-family: 'Minecraft'; color:#999;">Server Address</p>
            <div class="input-group text-center mb-3 mc-ip-address">
                <input readonly type="text" class="form-control mc-ip" value="{{$serverIp}}" aria-label="" aria-describedby="button-addon2">
                <div class="input-group-append">
                    <button class="btn-mc d-none d-lg-block btn-copy-ip" type="button" id="button-addon2" style="color:white; height:37px;"><span>Copy</span></button>
                </div>
            </div>
            
            <p style="font-family: 'Minecraft'; color:#999;">OR</p>
            <button class="btn-mc" type="button" style="width:200px;" onclick="window.location='{{url('register')}}';">Register Now</button>
            <p class="subtitle" style="margin-top: 15px;">Web registration is needed in order to unlock more in-game features.</p>

            <div>
                <span style="font-family: 'Minecraft'; color:#aaa; font-size: 1.25em;">
                    Currently Online Players: 
                    @if ($serverPlayerCount > $serverMaxPlayer-10)
                        <span style="color:#ffaa00;">{{$serverPlayerCount}}</span>/{{$serverMaxPlayer}}
                    @else
                        <span style="color:#55ff55;">{{$serverPlayerCount}}</span>/{{$serverMaxPlayer}}
                    @endif
                    
            
            </span>
            </div>
            
        </div>

        {{-- Server Desc --}}
        <div class="container-fluid section-desc text-center">
            <div class="row" style="margin:0 0;">
                <div class="col-md-3 offset-md-1 desc-item">
                    <h4>{{$title}}</h4>
                    <hr class="hr-light">
                    <p class="desc">
                        {{$title}} is a simple and a straight Minecraft survival multiplayer server.
                        As our goal, simplicity and easy access is our goal to help and welcome our players.
                    </p>
                </div>
                <div class="col-md-3 desc-item">
                    <h4>Survival</h4>
                    <hr class="hr-light">
                    <p class="desc">
                        Our primary gameplay is survival mode. 
                        Player can freely play and have a lot of experience.
                        Complete your experience with plots, jobs, and many more.
                    </p>
                </div>
                <div class="col-md-5 desc-bg">
                    
                </div>

            </div>  
        </div>

        {{-- Server Progress and Donation --}}
        <div class="container-fluid section-progress text-center">
            <div class="row">
                <div class="col-md-6 mc-progress-item mc-progress-right-border text-center">
                    <h4 class="text-center">Server Progress</h4>
                    <hr class="hr-light">
                    <p class="text-left">Progress: <span style="color:{{$serverProgressColor}}"><b>{{$serverProgress}}</b></span></p>
                    <div class="progress" style="">
                        <div class="progress-bar progress-bar-striped progress-bar-animated bg-success" role="progressbar" style="width: {{$serverProgressPercent}}%" aria-valuenow="{{$serverProgressPercent}}" aria-valuemin="0" aria-valuemax="100">{{$serverProgressPercent}}%</div>
                    </div>
                    
                    <p class="text-center mt-3">
                        We are opening beta registration to test our features. 
                        You can register yourself to be a beta tester here, click on the button below.
                    </p>
                    <a class="btn btn-success" href="{{url('register')}}">Beta Registration</a>
                </div>
                <div class="col-md-6 mc-progress-item">
                    <h4>Donations</h4>
                    <hr class="hr-light">
                    <p>
                        We opened donation system for who wants to donate and help us keep the server up.
                        Get additional bonuses for each donation.
                    </p>
                    
                <a class="btn btn-primary" href="{{url('donation')}}">Open Donations</a>

                </div>
            </div>
        </div>

        {{-- Contect Us --}}
        <div class="section-contact d-none d-lg-flex justify-content-center">
            <div class="card">
                <div class="row">
                    <div class="col-lg-4">
                        <div class="card-body text-center contact-bg text-white">
                            <h4>Contact Us</h4>
                            <hr class="hr-short">
                        </div>
                    </div>
                    <div class="col-lg mc-contact-form">
                        <form id="contact-form" name="contact-form" action="mail.php" method="POST">
                            <div class="form-row">
                                <div class="form-group col-md-6">
                                    <label for="txtEmail">Email address</label>
                                    <input type="email" class="form-control" id="txtEmail" aria-describedby="emailHelp" placeholder="Enter email">
                                </div>
                                <div class="form-group col-md-6">
                                    <label for="txtName">Full Name</label>
                                    <input type="text" class="form-control" id="txtName" placeholder="Enter Name">
                                </div>
                            </div>
                            <div class="form-row">
                                <div class="form-group col-md">
                                    <label for="txtSubject">Subject</label>
                                    <input type="text" class="form-control" id="txtSubject" placeholder="Enter Subject">
                                </div>
                            </div>
                            <div class="form-row">
                                <div class="form-group col-md">
                                    <label for="txtMessage">Your Message</label>
                                    <textarea class="form-control" id="txtMessage" rows="3"></textarea>
                                </div>
                            </div>
                            <button type="submit" class="btn btn-primary pull-right">Send</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>

        @include('templates/overlay_discord')
        @include('templates/footer')
    </body>
    <script>
        $(document).ready(function(){
            $('.mc-banner').parallax({
                imageSrc:"{{asset('/assets/img/bg_banner.png')}}",
                naturalWidth: 1920,
                naturalHeight: 1080
            });
            jQuery(window).trigger('resize').trigger('scroll');

            $('.btn-copy-ip').click(function(){
                $('input.mc-ip').select();
                document.execCommand("copy");

            });
        });


    </script>
    @include('templates/bs_foot')
</html>