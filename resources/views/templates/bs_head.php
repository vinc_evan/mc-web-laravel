<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="csrf-token" content="{{ csrf_token() }}">
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
<link href="<?php echo asset('/assets/css/personal.css');?>" rel="stylesheet">
<link href="<?php echo asset('/assets/css/minecraft.css');?>" rel="stylesheet">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.9.0/css/solid.min.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.9.0/css/brands.min.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.9.0/css/fontawesome.min.css">

<!-- <script src="https://kit.fontawesome.com/d8a32ee5ef.js"></script> -->
<!-- <link rel="stylesheet" media="screen" href="https://fontlibrary.org/face/minecraftia" type="text/css"/> -->
<link rel="icon" href="<?php echo asset('/assets/img/favicon.png');?>">

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.0/jquery.min.js"></script>

<script>


    //Change navbar bg color on scrolled. 
    $(function () {
        $(document).scroll(function () {
            var $nav = $("#main-navbar");
            $nav.toggleClass('scrolled', $(this).scrollTop() > $nav.height());
        });
        // $(".navbar-toggler").click(function (){
        //     var $nav = $("#main-navbar");
        //     $nav.toggleClass('scrolled');
        // });
    });



</script>