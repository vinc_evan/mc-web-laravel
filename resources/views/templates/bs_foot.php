<script>

    $('.overlay-close-btn').click(function(){
        $('.overlay').fadeOut(300);
    });

    //Discord Button overlay on footer.
    $('.discord-link').click(function(){
        $('.overlay-discord').fadeIn(300);

    });


    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

</script>


<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
<script src="<?php echo asset('/assets/js/scroller.js');?>"></script>
<script src="<?php echo asset('/assets/js/parallax.min.js');?>"></script>