<!--Navigation Bar Inner-->
<a class="navbar-brand" href="{{url('/home')}}">{{$title}}</a>
<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarTogglerDemo02" aria-controls="navbarTogglerDemo02" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
</button>
<div class="collapse navbar-collapse" id="navbarTogglerDemo02">
    <ul class="navbar-nav ml-auto mt-2 mt-lg-0 ">
        {{-- <li class="nav-item">
            <a class="nav-link page-scroll" href="{{url('donation')}}">Donation</a>
        </li>
        <li class="nav-item">
            <a class="nav-link page-scroll" href="{{url('/forum')}}">Forums</a>
        </li> --}}
        <li class="nav-item">
            <a class="nav-link page-scroll" href="{{url('dashboard')}}">Account</a>
        </li>
    </ul>
</div>
<!--End of Nav Bar-->