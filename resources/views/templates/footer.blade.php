<footer class="page-footer font-small mc-footer">
    <div class="container-fluid" style="background-color: #333;">
        <!-- Grid row-->
        <div class="row">
            <!-- Grid column -->
            <div class="col-md-12 py-5 ">
                <div class="mb-0 d-flex justify-content-center">
                <!-- Facebook -->
                    <a class="fb-ic">
                        <i class="fab fa-facebook-f mr-md-5 mr-3 fa-2x"> </i>
                    </a>
                
                    <!--Pinterest-->
                    <a class="pin-ic discord-link" href="javascript:void(0);">
                        <i class="fab fa-discord fa-2x"> </i>
                    </a>
                </div>
            </div>
            <!-- Grid column -->
        </div>
        <!-- Grid row-->
    </div>    
    <!-- Copyright -->
    <div class="footer-copyright text-center py-3">
        © 2019 Copyright <a href="#">{{$title}}</a>
    </div>
    <!-- Copyright -->
</footer>