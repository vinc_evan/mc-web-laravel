<!DOCTYPE html>
    <head>
        <title>{{$title}} | Donation</title>
        @include('templates/bs_head')
        <link rel="stylesheet" href="{{asset('/assets/css/staticpage.css')}}">
    </head>

    <body>
        <!--Navigation Bar-->
        <nav id="main-navbar" class="navbar navbar-expand-sm navbar-dark ">
            @include('templates/navbar-inner')
        </nav>
        <!--End of Nav Bar-->

        {{-- Banner and Page Title --}}
        <div class="container-fluid d-flex align-items-center mc-pg-banner">
            <div class="row">
                <div class="col text-white">
                    <h2>Donation</h2>
                </div>
            </div>
        </div>

        {{-- Pricing --}}
        <div class="text-center container p-5">
            <h3>Choose your package</h3>
            <hr class="hr-short" style="border-top:2px solid #777;"> 
        </div>
        <section class="pricing py-5">
            <div class="container section-pricing">
                <div class="row">
                    <!-- Plus Tier -->
                    <div class="col-lg-4">
                        <div class="card mb-5 mb-lg-0" style="height:600px;">
                        <div class="card-body">
                            <h4 class="card-title text-uppercase text-center text-warning">Plus</h4>
                            <h6 class="text-center text-muted"><del>Rp50,000/mo</del></h6>
                            <h6 class="card-price text-center">Rp30,000<span class="period">/mo</span></h6>
                            <hr>
                            <ul class="fa-ul">
                                <li><span class="fa-li"><i class="fa fa-check"></i></span><span class="text-warning">[Plus]</span> Name Tag</li>
                                <li><span class="fa-li"><i class="fa fa-check"></i></span>Colored and Formatted Chat</li>
                                <li><span class="fa-li"><i class="fa fa-check"></i></span>/workbench and /enderchest</li>
                                <li><span class="fa-li"><i class="fa fa-check"></i></span>Cheaper LWC price</li>
                                <li><span class="fa-li"><i class="fa fa-check"></i></span>Jobs EXP boost (0.15x)</li>
                                <li class="text-muted"><span class="fa-li"></span>More to come...</li>
                            </ul>
                            <a href="javascript:void(0);" class="donate-link btn btn-block btn-primary text-uppercase">Donate</a>
                        </div>
                        </div>
                    </div>
                    <!-- VIP Tier -->
                    <div class="col-lg-4">
                        <div class="card mb-5 mb-lg-0" style="height:600px;">
                        <div class="card-body">
                            <h5 class="card-title text-success text-uppercase text-center ">VIP</h5>
                            <h6 class="text-center text-muted"><del>Rp80,000/mo</del></h6>
                            <h6 class="card-price text-center">Rp50,000<span class="period">/mo</span></h6>
                            <hr>
                            <ul class="fa-ul">
                                <li><span class="fa-li"><i class="fa fa-check"></i></span><span class="text-success">[VIP]</span> Name Tag</li>
                                <li><span class="fa-li"><i class="fa fa-check"></i></span>Plus Features</li>
                                <li><span class="fa-li"><i class="fa fa-check"></i></span>Daily VIP Kits</li>
                                <li><span class="fa-li"><i class="fa fa-check"></i></span>Free LWC Price</li>
                                <li><span class="fa-li"><i class="fa fa-check"></i></span>Jobs EXP boost (0.20x)</li>
                                <li><span class="fa-li"><i class="fa fa-check"></i></span>Jobs Point boost (0.10x)</li>
                                <li><span class="fa-li"><i class="fa fa-check"></i></span>$10,000 In-Game Bonus</li>
                                <li class="text-muted"><span class="fa-li"></span>More to come...</li>
                            </ul>
                            <a href="javascript:void(0);" class="donate-link btn btn-block btn-primary text-uppercase">Donate</a>
                        </div>
                        </div>
                    </div>
                    <!-- Elite Tier -->
                    <div class="col-lg-4">
                        <div class="card" style="height:600px;">
                            <div class="card-body">
                                <h5 class="card-title text-info text-uppercase text-center">Elite</h5>
                                <h6 class="text-center text-muted"><del>Rp110,000/mo</del></h6>
                                <h6 class="card-price text-center">Rp85,000<span class="period">/mo</span></h6>
                                <hr>
                                <ul class="fa-ul">
                                    <li><span class="fa-li"><i class="fa fa-check"></i></span><span class="text-info">[Elite]</span> Name Tag</li>
                                    <li><span class="fa-li"><i class="fa fa-check"></i></span>VIP Features</li>
                                    <li><span class="fa-li"><i class="fa fa-check"></i></span>Daily Elite Kits</li>
                                    <li><span class="fa-li"><i class="fa fa-check"></i></span>Jobs EXP boost (0.20x)</li>
                                    <li><span class="fa-li"><i class="fa fa-check"></i></span>Jobs Point boost (0.15x)</li>
                                    <li><span class="fa-li"><i class="fa fa-check"></i></span>Skin Change</li>
                                    <li><span class="fa-li"><i class="fa fa-check"></i></span>Join <span class="text-warning">Explorer</span> and <span class="text-success">Adventurer</span> Jobs**</li>
                                    <li><span class="fa-li"><i class="fa fa-check"></i></span>$15,000 In-Game Bonus</li>
                                    <li class="text-muted"><span class="fa-li"></span>More to come...</li>
                                </ul>
                                <a href="javascript:void(0);" class="donate-link btn btn-block btn-primary text-uppercase">Donate</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="container section-pricing" style="padding-top:3%;">
                <div class="row">
                    <!-- Prime Tier -->
                    <div class="col-lg-4">
                        <div class="card mb-5 mb-lg-0" style="height:600px;">
                        <div class="card-body">
                            <h5 class="card-title text-success text-uppercase text-center">Prime</h5>
                            <h6 class="text-center text-muted"><del>Rp130,000/mo</del></h6>
                            <h6 class="card-price text-center">Rp100,000<span class="period">/mo</span></h6>
                            <hr>
                            <ul class="fa-ul">
                                <li><span class="fa-li"><i class="fa fa-check"></i></span><span class="text-success">[Prime]</span> Name Tag</li>
                                <li><span class="fa-li"><i class="fa fa-check"></i></span>Elite Features</li>
                                <li><span class="fa-li"><i class="fa fa-check"></i></span>Daily Prime Kits</li>
                                <li><span class="fa-li"><i class="fa fa-check"></i></span>Jobs Point boost (0.20x)</li>
                                <li><span class="fa-li"><i class="fa fa-check"></i></span>$18,000 In-Game Bonus</li>
                                <li class="text-muted"><span class="fa-li"></span>More to come...</li>
                            </ul>
                            <a href="javascript:void(0);" class="donate-link btn btn-block btn-primary text-uppercase">Donate</a>
                        </div>
                        </div>
                    </div>
                    <!-- Plus Tier -->
                    <div class="col-lg-4" style="height:550px;">
                        <h4 class="text-muted text-center" style="margin-top:225px;">More to come...</h4>
                    </div>
                </div>
            </div>
            <div class="container mt-5 text-center">
                <span class="text-muted">**Jobs can be transfered from your old job, contact online admin.</span><br>
                <a href="javascript:void(0);" class="text-info kits-link">Daily Kits Information</a>
            </div>
        </section>
        <div class="text-center container-fluid py-5 text-white" style="background-color:#333;">
            <div class="row">
                <div class="col">
                    <h4>Donating</h4><hr class="hr-light ">
                    <p class="mc-donation-text ">
                        Hello! We are a new and growing server! We need your help to keep our server online.<br>
                        A way to help us is via donations, and we'll give you additional rewards with it.
                    </p>
                </div>
            </div>
            <div class="row d-flex flex-column justify-content-center py-5">
                <h4>Goal</h4>
                <p>We are currently have to rent a server and keep this game going online!<br>Below is our goal that we have to achieve.</p>
                <div class="col-12 col-md-4 align-self-center mt-3">
                    <h6>3 months server (8G/4CPU)</h6>
                    <div class="progress">
                        <div class="progress-bar progress-bar-striped progress-bar-animated" role="progressbar" aria-valuenow="{{$donationPercentage}}" aria-valuemin="0" aria-valuemax="100" style="width: {{$donationPercentage}}%"></div>
                    </div>
                    <div class="d-flex justify-content-between">
                        <span>Rp{{$donationProgress}}</span>
                        <span>Rp{{$donationGoal}}</span>
                    </div>
                    <div>
                        <span>Collected: Rp{{$donationProgress}} ({{sprintf("%.2f", $donationPercentage)}}%)</span>
                    </div>
                </div>
            </div>
            <div class="row d-flex flex-column justify-content-center py-5">
                <h5>Our Top Donators</h5>
                <span class="text-muted"><i>You will be here later, thank you for your support ;)</i></span>
            </div>


        </div>
        <div class="overlay overlay-donate" onclick="">
            <div class=" overlay-inner">
                <div class="card ">
                    <h5 class="card-header">Donating</h5>
                    <div class="card-body">
                        <h5 class="card-title">Hello!</h5>
                        <p class="card-text">If you are Indonesian and want to donate and help the server, contact us at</p>
                        <p class="card-text">Email: evangunawan@live.com<br>WhatsApp/SMS: +62 812 1447 4854</p>
                        <p class="card-text">Currently, we are accepting bank transfer (BCA) and OVO Only.</p>
                        <a href="javascript:void(0);" class="overlay-close-btn btn btn-primary">I understand</a>
                    </div>
                </div>
            </div>
            
        </div>
        <div class="overlay overlay-kits" onclick="">
            <div class="overlay-close overlay-close-btn"><i class="fas fa-times"></i></div>
            <div class="overlay-inner w-50">
                <div class="card">
                    <h5 class="card-header">Daily Kits</h5>
                    <div class="card-body">
                        <div class="row">
                            <div class="col-6">
                                <h5 class="card-title">Plus</h5>
                                <span class="card-text">1x Iron Pickaxe</span><br>
                                <span class="card-text">1x Iron Axe</span><br>
                                <span class="card-text">16x Breads</span><br>
                                <span class="card-text">4x Steak</span><br>
                                <span class="card-text">6x Iron Ingot</span><br>
                            </div>
                            <div class="col-6">
                                <h5 class="card-title">VIP</h5>
                                <span class="card-text">1x Iron Sword</span><br>
                                <span class="card-text">1x Iron Pickaxe</span><br>
                                <span class="card-text">1x Iron Axe</span><br>
                                <span class="card-text">12x Apples</span><br>
                                <span class="card-text">8x Steaks</span><br>
                                <span class="card-text">12x Iron Ingot</span><br>
                            </div>
                        </div>
                        <div class="row mt-5">
                            <div class="col-6">
                                <h5 class="card-title">Elite</h5>
                                <span class="card-text">1x Iron Sword</span><br>
                                <span class="card-text">1x Iron Pickaxe</span><br>
                                <span class="card-text">1x Iron Axe</span><br>
                                <span class="card-text">12x Steaks</span><br>
                                <span class="card-text">12x Apples</span><br>
                                <span class="card-text">4x Diamonds</span><br>
                            </div>
                            <div class="col-6">
                                <h5 class="card-title">Prime</h5>
                                <span class="card-text">1x Iron Sword</span><br>
                                <span class="card-text">1x Iron Pickaxe</span><br>
                                <span class="card-text">1x Iron Axe</span><br>
                                <span class="card-text">20x Steaks</span><br>
                                <span class="card-text">1x Emerald</span><br>
                                <span class="card-text">6x Diamonds</span><br>
                                <span class="card-text">12x Iron Ingot</span>
                            </div>
                        </div>
                        <div class="row mt-5">
                            <div class="col text-center">
                                <p class="text-secondary">Kits contents will always be updated and changed later</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            
        </div>
        @include('templates/overlay_discord')
        @include('templates/footer')
    </body>
    @include('templates/bs_foot')
    <script>
        $(".donate-link").click(function(){
            $(".overlay-donate").fadeIn(300);
        });
        $(".kits-link").click(function(){
            $(".overlay-kits").fadeIn(300);
        });
    </script>

</html>