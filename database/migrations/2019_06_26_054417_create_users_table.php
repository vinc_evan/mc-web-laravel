<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;


class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('username',32)->unique();
            $table->string('email',64);
            $table->string('fullname',64);
            $table->string('password');
            $table->rememberToken();
            $table->timestamps();

            // Authme
            $table->string('amIp',40)->nullable();
            $table->string('amRealName')->nullable();
            $table->bigInteger('amLastlogin')->default(round(microtime(true) * 1000));
            $table->bigInteger('amRegdate')->default(round(microtime(true) * 1000));
            $table->smallInteger('amIsLogged')->default(0);
            $table->smallInteger('amHasSession')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
