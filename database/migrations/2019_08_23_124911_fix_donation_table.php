<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class FixDonationTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('donations', function (Blueprint $table) {

            $table->bigIncrements('id')->first()->autoIncrement();
            $table->renameColumn('donation_tier', 'donate_current_tier');
            $table->renameColumn('donate_time', 'donate_start');
            $table->bigInteger('donate_end');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('donations', function (Blueprint $table) {
            $table->dropPrimary('id');
            $table->dropColumn('id');
            $table->dropColumn('donate_end');
            $table->renameColumn('donate_current_tier', 'donation_tier');
            $table->renameColumn('donate_start', 'donate_time');
        });
    }
}
