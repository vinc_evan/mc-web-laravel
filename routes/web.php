<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

Route::get('/','Home@show');
Route::get('/home','Home@show');
Route::post('/home','Home@getData')->name('store.data');
Route::post('/','Home@getData')->name('store.data');
// Route::redirect('/','/mc-web/home');

Route::get('password/change', 'Auth\AuthController@changePassword')->name('password/change');;
Route::post('password/change', 'Auth\AuthController@postChangePassword');

Route::redirect('/forum','/forum');

Route::get('/donation','DonationController@show');

Auth::routes();

Route::get('/dashboard', 'DashboardController@index')->name('dashboard');
Route::get('/dashboard/changepassword', 'DashboardController@showPasswordChange');

Route::get('/logout','DashboardController@logout');

Route::get('/donationTest','DashboardController@giveDonation');
