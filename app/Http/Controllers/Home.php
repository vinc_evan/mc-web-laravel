<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class Home extends Controller
{
    private $data = array(
        'serverIp' => "mc.nixnetwork.id",
        'serverProgress' => "Beta Stage",
        'serverProgressPercent' => 70,
        'serverProgressColor' => '#00d100',
        'serverPlayerCount' => '-1',
        'serverMaxPlayer'=> '25'
    );

    public function show(){
        $this->data['serverPlayerCount'] = Storage::get("playerCount.txt");
        return view('home')->with($this->data);
    }

    public function getData(Request $request){

        $regist_det = array(
            'mail'=>$request->mail,
            'uname'=>$request->uname
        );
        return view('auth.register')->with($regist_det);
    }

    public function updatePlayerCount(Request $request){
        Storage::put("playerCount.txt", $request->post());
    }
}
