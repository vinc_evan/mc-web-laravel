<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class DonationController extends Controller
{
    private $data = array(
        'donationProgress' => 350000,
        'donationGoal' => 1700000,
    );

    private function calculateDonationPercentage($progress, $goal){
        return $progress/$goal * 100;
    }

    public function show(){
        $this->data['donationPercentage'] = $this->calculateDonationPercentage($this->data['donationProgress'],$this->data['donationGoal']);
        return view('donation')->with($this->data);
    }
}
