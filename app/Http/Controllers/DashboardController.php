<?php

namespace App\Http\Controllers;

use App\User;
use App\Donation;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class DashboardController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $user_data = \DB::table('user_data')->where('user_id',Auth::user()->id)->first();
        $donate_data = \DB::table('donations')->where('user_id',Auth::user()->id)->first();
        return view('dashboard',['user_data' => $user_data,'donate_data'=>$donate_data]);
    }

    public function showPasswordChange(){
        return view('auth/change_password');
    }

    // public function giveDonation(){
    //     $user_donation = \DB::table('donations')->where('user_id',Auth::user()->id)->first();
    //     if($user_donation == null){
    //         $donation =  Donation::create([
    //             'user_id' => Auth::user()->id,
    //             'donation_tier' => 'Tier',
    //         ]);
    //     }else{
    //         \DB::table('donations')->where('user_id',Auth::user()->id)->update(['donation_tier'=>'Tier2']);
    //     }
    // }

    public function logout(){
        auth()->logout();
        // redirect to homepage
        return redirect('/');
    }
}
