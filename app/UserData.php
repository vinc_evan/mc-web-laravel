<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserData extends Model
{
    protected $table = 'user_data';

    protected $fillable = ['user_id'];

    protected $attributes = [
        'economy' => 100,
    ];

    public function user(){
        return $this->belongsTo('App\User');
    }
}
