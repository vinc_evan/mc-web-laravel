<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Donation extends Model
{
    public $timestamps = false;

    protected $table = 'donations';

    protected $fillable = ['user_id','donate_current_tier'];

    public function user(){
        return $this->belongsTo('App\User');
    }
}
