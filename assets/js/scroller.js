$('.page-scroll').on('click', function(e){
    var targetId = $(this).attr('href');
    
    var targetElement = $(targetId);
    
    // console.log(targetElement.offset().top);
    // $(window).scrollTop(targetElement.offset().top);
    $("html, body").animate({
        scrollTop : targetElement.offset().top -70
    },500,'swing');

    e.preventDefault();
});